/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
require('bootstrap');
// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';
// create global $ and jQuery variables
global.$ = global.jQuery = $;
var $newLinkLi = $('<li></li>');

jQuery(document).ready(function() {

    var $collectionHolder = $('#sondages_form_reponses');
    var $addQuestionButton = $('.add-reponse-sondage');
    var $registerButton = $('.register');
    var $loginButton = $('.login');
    $collectionHolder.data('index', $collectionHolder.find('input').length);
    $registerButton.on('click', function(e) {
        console.log('clic registerbutton ');

    });
    $loginButton.on('click', function(e) {
        console.log('clic loginbutton ');

    });
    $addQuestionButton.on('click', function(e) {
        console.log('clic addquestion ');
        addTagForm($collectionHolder, $addQuestionButton);
    });
});

function addTagForm($collectionHolder, $newLinkLi) {
    var prototype = $collectionHolder.data('prototype');

    var index = $collectionHolder.data('index');
    var newForm = prototype;
    console.log('addTagform');
    newForm = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index + 1);
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}


console.log('Hello Webpack Encore! Edit me in assets/app.js');
