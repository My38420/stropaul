var $newLinkLi = $('<li></li>');

jQuery(document).ready(function() {

    var $collectionHolder = $('#sondages_form_reponses');
    var $addQuestionButton = $('.add-reponse-sondage');
    $collectionHolder.data('index', $collectionHolder.find('input').length);

    $addQuestionButton.on('click', function(e) {
        console.log('clic addquestion ');
        addTagForm($collectionHolder, $addQuestionButton);
    });
});

function addTagForm($collectionHolder, $newLinkLi) {
    var prototype = $collectionHolder.data('prototype');

    var index = $collectionHolder.data('index');
    var newForm = prototype;

    newForm = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index + 1);
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}

