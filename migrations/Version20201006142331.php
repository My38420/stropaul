<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201006142331 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reponses ADD sondages_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE reponses ADD CONSTRAINT FK_1E512EC665C3BD4A FOREIGN KEY (sondages_id) REFERENCES sondages (id)');
        $this->addSql('CREATE INDEX IDX_1E512EC665C3BD4A ON reponses (sondages_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reponses DROP FOREIGN KEY FK_1E512EC665C3BD4A');
        $this->addSql('DROP INDEX IDX_1E512EC665C3BD4A ON reponses');
        $this->addSql('ALTER TABLE reponses DROP sondages_id');
    }
}
