<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201013151606 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_reponses (user_id INT NOT NULL, reponses_id INT NOT NULL, INDEX IDX_FEDCCAC6A76ED395 (user_id), INDEX IDX_FEDCCAC6E4084792 (reponses_id), PRIMARY KEY(user_id, reponses_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_reponses ADD CONSTRAINT FK_FEDCCAC6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_reponses ADD CONSTRAINT FK_FEDCCAC6E4084792 FOREIGN KEY (reponses_id) REFERENCES reponses (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_reponses');
    }
}
