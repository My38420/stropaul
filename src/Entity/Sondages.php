<?php

namespace App\Entity;

use App\Repository\SondagesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @ORM\Entity(repositoryClass=SondagesRepository::class)
 */
class Sondages
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $question;

    /**
     * @ORM\OneToMany(targetEntity=Reponses::class, mappedBy="sondages",cascade={"persist","remove"})
     */
    private $reponses;

    /**
     * @ORM\Column(type="boolean")
     */
    private $type_reponse=false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $type_sondage;

    public function __construct()
    {
        $this->reponses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return Collection|Reponses[]
     */
    public function getReponses(): Collection
    {
        return $this->reponses;
    }

    public function addReponse(Reponses $reponse): self
    {
        if (!$this->reponses->contains($reponse)) {
            $this->reponses[] = $reponse;
            $reponse->setSondages($this);
        }

        return $this;
    }

    public function removeReponse(Reponses $reponse): self
    {
        if ($this->reponses->contains($reponse)) {
            $this->reponses->removeElement($reponse);
            // set the owning side to null (unless already changed)
            if ($reponse->getSondages() === $this) {
                $reponse->setSondages(null);
            }
        }

        return $this;
    }

    public function getTypeReponse():bool
    {
        return $this->type_reponse;
    }

    public function setTypeReponse(bool $type_reponse): self
    {
        $this->type_reponse = $type_reponse;

        return $this;
    }

    public function getTypeSondage(): ?bool
    {
        return $this->type_sondage;
    }

    public function setTypeSondage(bool $type_sondage): self
    {
        $this->type_sondage = $type_sondage;

        return $this;
    }

}
