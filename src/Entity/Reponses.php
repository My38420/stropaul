<?php

namespace App\Entity;

use App\Repository\ReponsesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReponsesRepository::class)
 */
class Reponses
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle_reponse;

    /**
     * @ORM\ManyToOne(targetEntity=Sondages::class, inversedBy="reponses")
     */
    private $sondages;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="vote")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleReponse(): ?string
    {
        return $this->libelle_reponse;
    }

    public function setLibelleReponse(string $libelle_reponse): self
    {
        $this->libelle_reponse = $libelle_reponse;

        return $this;
    }

    public function getSondages(): ?Sondages
    {
        return $this->sondages;
    }

    public function setSondages(?Sondages $sondages): self
    {
        $this->sondages = $sondages;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addVote($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeVote($this);
        }

        return $this;
    }
}
