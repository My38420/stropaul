<?php

namespace App\Controller;
use App\Entity\Reponses;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\ReponsesRepository;
use Symfony\Component\Routing\Annotation\Route;

class ReponsesController extends AbstractController
{
    /**
     * @Route("/ajoutVote/{id}", name="ajoutVote")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     */
    public function ajoutVote(Request $request, EntityManagerInterface  $entityManager, $id): Response
    {
        $ReponsesRepository = $entityManager->getRepository(Reponses::class);
        $Reponse = $ReponsesRepository->find($id);

     }
    /**
     * @Route("/reponses", name="reponses")
     */
    public function index()
    {
        return $this->render('reponses/index.html.twig', [
            'controller_name' => 'ReponsesController',
        ]);
    }
}
