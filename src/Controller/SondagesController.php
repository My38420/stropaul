<?php

namespace App\Controller;

use App\Entity\Reponses;
use App\Entity\Sondages;
use App\Entity\User;
use App\Form\SondagesFormType;
use App\Repository\ReponsesRepository;
use App\Repository\SondagesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\ChoiceList\ChoiceList;

class SondagesController extends AbstractController
{
    /**    fonction de création d'un sondage
     * @Route("/newSondage", name="newSondage")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newSondage(Request $request, EntityManagerInterface $entityManager)

    {
        $sondage = new Sondages();
        $form = $this->createForm(SondagesFormType::class, $sondage);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($sondage);

            $entityManager->flush();
            return $this->render('sondages/index.html.twig');
        }

        return $this->render('pages/sondages.html.twig', [
            'myForm' => $form->createView()
        ]);
    }
    /**   /* fonction qui liste tous les sondages
     * @Route("/listSondage", name="listSondage")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function listSondage(SondagesRepository $sondagesRepository)
    {
          $arraySondages = $sondagesRepository -> findAll();
             return $this->render('pages/liste_sondages.html.twig',['sondages' => $arraySondages]);
    }
    /**     retour sur la page cu choix d'action
     * @Route("/retActions", name="retActions")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function retActions()
    {
       return $this->render('sondages/index.html.twig');
    }
    /**  modification d'un sondage via son id
     * @Route("/editSondage/{id}", name="editSondage", methods={"GET","POST"})
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function editSondage(Request $request, Sondages $sondage): Response
    {
        $form = $this->createForm(SondagesFormType::class, $sondage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('listSondage');
        }

        return $this->render('sondages/edit.html.twig', [
            'sondage' => $sondage,
            'form' => $form->createView(),
        ]);
    }

    /**  affichage des résultats d'un sondage via son id
     * @Route("/resSondage/{id}", name="resSondage")
     * @param EntityManagerInterface $entityManager
     */
    public function resSondage(EntityManagerInterface  $entityManager, $id)
    {
        $SondagesRepository = $entityManager->getRepository(Sondages::class);
        $Sondage = $SondagesRepository->find($id);
        $reponses =$Sondage->getReponses();
        $nbuser = 0;
        foreach ($reponses as $reponse) {

                $users = $reponse->getUsers();
                $nbvote = 0;

                foreach ($users as $user) {
                    $nbvote = $nbvote + 1;

                    }
            $nbuser = $nbuser + $nbvote;

            $repid = $reponse -> getLibelleReponse();
                $tabvote [$repid] = $nbvote;


        }

        return $this->render('sondages/resultats.html.twig',
            [
                'sondage' => $Sondage,
                'tabvote'=> $tabvote,
                'nbvotes'=> $nbuser

            ]);

    }
    /**  vote d'un sondage via son id
     * @Route("/voteSondage/{id}", name="voteSondage")
     * @param EntityManagerInterface $entityManager
     */
    public function voteSondage(Request $request, EntityManagerInterface  $entityManager, $id): Response
    {
        $SondagesRepository = $entityManager->getRepository(Sondages::class);
        $Sondage = $SondagesRepository->find($id);
        $reponses = $Sondage->getReponses();
        $multiple = $Sondage->getTypeReponse();
        $existuser = false;
        foreach ($reponses as $reponse) {
            {
                $users = $reponse->getUsers();
                foreach ($users as $user) {
                    if ($user === $this->getUser()) {
                        $existuser = true;
                    }
                }
            }
        }

        if ($existuser) {
            return new response ('vous avez déjà voté pour ce sondage');
        } else {

            $builder = $this->createFormBuilder($Sondage);
            /*
             * option mapped = false pour lire à symfony que ce n'est pas
             * grave si on n'a pas un collection type pour reponses qui est fils
             * de sondage
             */
            $builder->add('reponses', ChoiceType::class, [
                'multiple' => $multiple,
                'expanded' => true,
                'choices' => $reponses,
                'choice_label' => 'libelle_reponse',
                'mapped' => false
            ]);
            $form = $builder->getForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $reponse = $form['reponses']->getData();
                if ($multiple) {
                    // pour chaque réponse choisie
                    foreach ($reponse as $reponsechoisie) {

                        $reponsechoisie->addUser($this->getUser());
                        $entityManager->persist($reponsechoisie);
                    }
                } else {
                    $reponse->addUser($this->getUser());
                    $entityManager->persist($reponse);
                }

                $entityManager->flush();

                return $this->redirectToRoute('listSondage');
            }

            return $this->render('sondages/vote_sondages.html.twig', [
                'sondage' => $Sondage,
                'myForm' => $form->createView(),
            ]);
        }
    }
    /**  suppression d'un sondage via son id
     * @Route("/delSondage/{id}", name="delSondage")
     */
    public function delSondage(EntityManagerInterface  $entityManager, $id)
    {
        $SondagesRepository = $entityManager->getRepository(Sondages::class);
            $Sondage = $SondagesRepository->find($id);
            $entityManager->remove($Sondage);
            $entityManager->flush();

        return $this->redirectToRoute('listSondage');
    }

     /**
     * @Route("/sondages", name="sondages")
     */
    public function index()
    {
        return $this->render('sondages/index.html.twig', [
            'controller_name' => 'SondagesController',
        ]);
    }
}
