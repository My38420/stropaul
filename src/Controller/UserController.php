<?php

namespace App\Controller;

use App\Entity\Sondages;
use App\Repository\SondagesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }
    /**
     * @Route("/mySondage/{id}", name="mySondage")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function mySondage(EntityManagerInterface $entityManager, $id)
    {
        $entityManager->getRepository(Sondages::class)->getvotesusers();

//        return $this->render('pages/mes_sondages.html.twig',['sondages' => $arraySondages]);
    }

}
