<?php

namespace App\Form;

use App\Entity\Reponses;
use App\Entity\Sondages;
use App\Form\ReponsesFormType;
use Cassandra\Tinyint;
use Doctrine\DBAL\Types\BooleanType;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SondagesFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class,
                ['attr' =>['class'=>'form-control'],
                ])
            ->add('question', TextType::class, ['attr' =>['class'=>'form-control']])
            ->add('type_reponse', CheckboxType::class, [
                'label' =>$options['typereponse'],
                'required'=>false]
            )
            ->add('type_sondage', CheckboxType::class, [
                'label' =>$options['typesondage'],
                'required'=>false])
            ->add('reponses', CollectionType::class, [
                  'entry_type' => ReponsesFormType::class,
                  'entry_options' => ['label'=>false],
                  'allow_add' => true,
                  'prototype' => true,
                  'by_reference' => false,
                  'allow_delete' => true
            ]);

        }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sondages::class,
            'typereponse'=>'cocher si choix multiple ',
            'typesondage'=>'cocher si sondage brouillon '
        ]);
    }
}
